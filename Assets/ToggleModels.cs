﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleModels : MonoBehaviour {
	public GameObject[] modelsToShow;
	public GameObject[] timeLinesToShow;

	int currentModel=0;
	bool oldButton=false;
	public uint whichButton;
	//bool oldKey=false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (MiddleVR.VRDeviceMgr != null) //make sure MiddleVR is loaded and initialized
		{
			bool currentButtonStatus = MiddleVR.VRDeviceMgr.IsWandButtonPressed (whichButton);

			if ((currentButtonStatus == true && oldButton == false)) // || (currentKeyStatus == true && oldKey == false))
			{
				print ("Toggling Now!");

				currentModel = currentModel + 1;

				if (currentModel >= modelsToShow.Length)
					currentModel = 0; //handle roll back to scene 0

				for (int i = 0; i < modelsToShow.Length; i++) //turn everything off
			 	    modelsToShow [i].SetActive (false);

				for (int i = 0; i < timeLinesToShow.Length; i++) //turn off all timelines
				{
					if (timeLinesToShow [i] != null)
						timeLinesToShow [i].SetActive (false);
				}

				modelsToShow [currentModel].SetActive (true);

				if(timeLinesToShow [currentModel]!=null) //make sure this is defined
					timeLinesToShow [currentModel].SetActive (true);
			}
			oldButton = currentButtonStatus;
		}
	}
}
