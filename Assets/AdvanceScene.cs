﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AdvanceScene : MonoBehaviour {

	int currentScene=0;
	bool oldButton=false;
	bool oldButton2=false;

	bool needPositionUpdate=true; //update start position when we very first startup
	Vector3 pos=new Vector3(0f,0f,0f);
	Quaternion quat=new Quaternion();

	public int []scenesToShow=new int[] {0,3,8,6};
	public uint advanceButton;

	public string ourName;
	public bool activeInThisScene=true;

	static Dictionary<string, AdvanceScene> instanceRefs = new Dictionary<string, AdvanceScene>(); //rewrote using dictionary to support anynumber of AdvanceScene Scripts

	void Awake()
	{
		if(instanceRefs.ContainsKey(ourName)==false)
		{
			instanceRefs[ourName] = this;
			DontDestroyOnLoad(gameObject);
		}else
		{
			DestroyImmediate(gameObject);
		}
	}

	// Use this for initialization
	void Start () {
		
	}

	void determineIfWeShouldBeActive()
	{
		int sceneWeAreIn=SceneManager.GetActiveScene ().buildIndex;
		activeInThisScene = false;


		for (int i = 0; i < scenesToShow.Length; i++)
		{
			if (scenesToShow [i] == sceneWeAreIn)
			{
				activeInThisScene = true;
				break;
			}
		}
	}

	// Update is called once per frame
	void Update () {
		if (MiddleVR.VRDeviceMgr != null) //make sure MiddleVR is loaded and initialized
		{
			determineIfWeShouldBeActive ();

			if (activeInThisScene == false)
				return;
			
			bool currentButtonStatus = MiddleVR.VRDeviceMgr.IsWandButtonPressed (advanceButton);

			if (currentButtonStatus == true && oldButton == false)
			{
				print ("Advancing Scene Now!");

				currentScene = currentScene + 1;

				if (currentScene>=scenesToShow.Length)
					currentScene = 0; //handle roll back to scene 0
			
				//SceneManager.LoadScene (scenesToShow[currentScene]);
				print ("  scene index is: " + currentScene);
				print ("  trying to show scene: " + scenesToShow [currentScene]);
				//Application.LoadLevel(scenesToShow[currentScene]);

				//lets store position / orientation
				GameObject cam=GameObject.Find("OurCenterNode");
				if (cam == null)
					Debug.LogError ("No CameraPosition in scene???");
				
				pos = cam.transform.position;
				quat = cam.transform.rotation;

				SceneManager.LoadScene (scenesToShow[currentScene], LoadSceneMode.Single);

				oldButton = currentButtonStatus;
				needPositionUpdate = true;

				return;
			}
				
			oldButton = currentButtonStatus;

			if (needPositionUpdate == true)
			{
				GameObject cam=GameObject.Find("OurCenterNode");
				if (cam == null)
					Debug.LogError ("No CameraPosition in scene???");

				print ("trying to set to position: " + pos);

				GameObject potentialStartLocation = GameObject.Find ("SceneStartLocation");

				if (potentialStartLocation != null) //if we have a defined SceneStartLocation, lets use it
				{
					cam.transform.position = potentialStartLocation.transform.position;
					cam.transform.rotation = potentialStartLocation.transform.rotation;
				}
				else //otherwise just go to last known position
				{
					cam.transform.position = pos;
					cam.transform.rotation = quat;
				}

				needPositionUpdate = false;
			}
		}
			
	}
}
